import {useState} from 'react';
import {Button, Col, Card, Row} from 'react-bootstrap';

export default function CourseCard({courseProp}) {
    // console.log(props.courseProp);
    // console.log(courseProp)

    const {name, description, price} = courseProp;

//Syntax
    //const [getter, setter] = useState(initialGetterValue)

const[seat, setSeat] = useState(30);


    const[count, setCount] = useState(0);
    console.log(useState(0));

    function enroll() {
    if(seat > 0) {    
        setSeat(seat - 1);    
        setCount(count + 1);
        console.log('Enrolees: ' + count)
        } else {
            alert('No more seats')
        }
    }

    return(
        <Row className = "mb-2">
            <Col>
                <Card className="cardHighlight p-3">
                    <Card.Body>
                        <Card.Title>{name}</Card.Title>
                            <Card.Subtitle>Description:</Card.Subtitle>
                            <Card.Text>{description}</Card.Text>
                            <Card.Subtitle>Price:</Card.Subtitle>
                            <Card.Text>Php {price}</Card.Text>
                            <Card.Text>Enrollees: {count}</Card.Text>
                            <Button variant="primary" onClick={enroll}>Enroll</Button>
                    </Card.Body>
                </Card>
            </Col>
        </Row>

    )

}