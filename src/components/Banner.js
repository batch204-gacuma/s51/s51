import { Button, Row, Col } from 'react-bootstrap';

export default function Banner(){
	return(
		<Row>
			<Col className="p-5 text-center">
				<h1>Zuit Coding Bootcamp</h1>
				<p>Oppurtunities for everyone, everywhere.</p>
				<Button variant="primary">Enroll now!</Button>
			</Col>
		</Row>
	)
}
